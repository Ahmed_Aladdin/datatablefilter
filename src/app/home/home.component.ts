import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import *  as  data from '../../assets/Data/Employees.json';
import { FormGroup, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private datePipe: DatePipe) { }
  employees: any = data.data.employees;
  displayedColumns: string[] = ['name', 'title', 'hiring date', 'salary'];
  dataSource: MatTableDataSource<any>

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  englishBtn = true;
  arabicBtn = false;

  filterForm = new FormGroup({
    filterBy: new FormControl(''),
    filterType: new FormControl(''),
    from: new FormControl(''),
    to: new FormControl(''),
    value: new FormControl('')
  })

  typeOfFilterValue = '';
  filterByValue = '';

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.employees);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getValueOfFilterType(value) {
    console.log('value', value);
    if (value == 'between') {
      this.typeOfFilterValue = 'between';
    } else {
      this.typeOfFilterValue = '';
    }
  }

  getValueOfFilterBy(value) {
    if (value == 'hiringDate') {
      this.filterByValue = 'date';
    } else {
      this.filterByValue = '';
    }
  }

  onSubmit() {
    let btnCloseModal: any = document.querySelectorAll('#closeModal');
    btnCloseModal[0].click();//close modal after submit filteration

    let column = this.filterForm.controls.filterBy.value;
    let method = this.filterForm.controls.filterType.value;
    let value = this.filterForm.controls.value.value;
    let from = this.filterForm.controls.from.value;
    let to = this.filterForm.controls.to.value;

    this.customizeFilter(column, method, value, from, to)
  }

  // customize function for filteration Modal
  customizeFilter(column, method, value, from, to) {
    let result = this.employees.filter((obj: any) => {
      if (column === 'firstContractingSalary') {
        if (method === 'before') {
          if (parseInt(value) > obj.firstContractingSalary)
            return obj
        }
        if (method === 'after') {
          if (parseInt(value) < obj.firstContractingSalary)
            return obj
        }
        if (method === 'between') {
          if (parseInt(from) <= obj.firstContractingSalary && parseInt(to) >= obj.firstContractingSalary)
            return obj
        }
      }
      else {
        if (method === 'before') {
          if (new Date(value).getTime() > new Date(obj.hiringDate).getTime())
            return obj
        }
        if (method === 'after') {
          if (new Date(value).getTime() < new Date(obj.hiringDate).getTime())
            return obj
        }
        if (method === 'between') {
          if (new Date(from).getTime() <= new Date(obj.hiringDate).getTime() && new Date(to).getTime() >= new Date(obj.hiringDate).getTime())
            return obj
        }
      }
    });

    this.dataSource = new MatTableDataSource(result);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  //toggle active btn arabic or english
  activeBtn(title) {
    if (title == 'english') {
      this.arabicBtn = false;
      this.englishBtn = true;
    }
    else {
      this.arabicBtn = true;
      this.englishBtn = false;
    }
  }
}


